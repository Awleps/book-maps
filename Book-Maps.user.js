// ==UserScript==
// @name           Book-Maps
// @namespace      http://tampermonkey.net/
// @version        0.1.201806232351
// @description    make G-Maps roads using Intel bookmarks
// @author         Awleps
// @updateURL      https://bitbucket.org/Awleps/book-maps/raw/master/Book-Maps.meta.js
// @downloadURL    https://bitbucket.org/Awleps/book-maps/raw/master/Book-Maps.user.js
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant        none
// ==/UserScript==

function wrapper(plugin_info) {
    "use strict";
    if(typeof window.plugin !== 'function') window.plugin = function(){};
    window.plugin.bookMaps = function(){};
    plugin_info.pluginId = "book-maps-by-awleps"
    window.plugin.bookMaps.showDialog = function(e) {
        e.preventDefault();

        // scan bookmarks and folders
        let bkmrks = {};
        for (let folder in window.plugin.bookmarks.bkmrksObj.portals) {
            for(let portalId in window.plugin.bookmarks.bkmrksObj.portals[folder].bkmrk) {
                let p = window.plugin.bookmarks.bkmrksObj.portals[folder].bkmrk[portalId];
                bkmrks[p.label] = p.latlng;
            }
        }

        // create form
        let wrapper = $('<div>');
        let form = $('<form>').attr('id', 'bookmaps-form').appendTo(wrapper);
        let start = $('<select>').attr('id', 'bookmaps-form-start').appendTo($('<label>').text('From').appendTo(form));
        let end = $('<select>').attr('id', 'bookmaps-form-end').appendTo($('<label>').text('To').appendTo(form));
        let travel_mode = $('<select>').attr('id','bookmaps-form-travelmode');
        travel_mode.append($('<option>').text("Driving")).append($('<option>').text("Walking")).appendTo($('<label>').text('Travel Mode').appendTo(form));
        let fs = $('<fieldset>').html('<label>Select Waypoints</label>').appendTo(form);
        let waypoints_from = $('<select>').attr('id', 'bookmaps-form-waypoints-from').attr('multiple', true).appendTo(fs);
        $('<a>').attr({ href: '#', id: 'bookmaps-form-add-waypoint'}).text('Add >>').appendTo(fs);
        $('<a>').attr({ href: '#', id: 'bookmaps-form-del-waypoint'}).text('<< Remove').appendTo(fs);
        let waypoints_to = $('<select>').attr('id', 'bookmaps-form-waypoints-to').attr('multiple', true).appendTo(fs);
        $('<a>').attr({ href: '#', id: 'bookmaps-form-up-waypoint'}).text('Move Up').appendTo(fs);
        $('<a>').attr({ href: '#', id: 'bookmaps-form-dn-waypoint'}).text('Move Down').appendTo(fs);
        let ok_btn = $('<button>').text('Generate Url').attr('type','submit').appendTo(form);
        $('<a>').attr('id', 'bookmaps-form-url').appendTo(form);
        // append list of portals in start, end adn waypoints
        for(let i in bkmrks){
            let option = $('<option>').val(bkmrks[i]).text(i);
            start.append(option);
            end.append(option.clone());
            waypoints_from.append(option.clone())
        }
        // create dialog
        dialog({
            html: wrapper.html(),
            id: 'dialog-bookMaps',
            title: 'BookMaps',
            dialogClass: 'ui-dialog-bookMaps'
        });
    }

    window.plugin.bookMaps.generateUrl = function() {
    /*
    ** Maps URL prototype
    ** https://www.google.com/maps/dir/?api=1&origin=<ll1>&destination=<ll2>&travelmode=<driving|walking|biycling>&waypoints=<wp1>|<wp2>
    */
        return 'https://www.google.com/maps/dir/?api=1&origin=<ll1>&destination=<ll2>&travelmode=<tvm>&waypoints=<wpnts>'
        .replace( '<ll1>', $('#bookmaps-form-start').val() )
        .replace( '<ll2>', $('#bookmaps-form-end').val() )
        .replace( '<tvm>', $('#bookmaps-form-travelmode').val() )
        .replace( '<wpnts>', ($('#bookmaps-form-waypoints-to').val() || []).join('|') );
    }
    const setup = function() {
        console.error($('#toolbar'))
        $('<style>').html(`
        #bookmaps-form select {width: 100%;display:block;}
        #bookmaps-form a { margin: 5px; }
        `).appendTo($('head'));
        $('body').on('submit', '#bookmaps-form', function(e) {
            e.preventDefault();
            let url = window.plugin.bookMaps.generateUrl;
            $('#bookmaps-form-url').attr('href', url).attr('target','_blank').text(url);
        }).on('click', '#bookmaps-form-add-waypoint', function(e) {
            e.preventDefault();
            $('#bookmaps-form-waypoints-from > :selected').appendTo($('#bookmaps-form-waypoints-to'));
        }).on('click', '#bookmaps-form-del-waypoint', function(e) {
            e.preventDefault();
            $('#bookmaps-form-waypoints-to > :selected').appendTo($('#bookmaps-form-waypoints-from'));
        }).on('click', '#bookmaps-form-up-waypoint', function(e) {
            e.preventDefault();
            $('#bookmaps-form-waypoints-to > :selected').each(function(){
                let new_index = $('#bookmaps-form-waypoints-to option').index(this) - 1;
                console.log(new_index);
                if(new_index > -1) {
                    $('#bookmaps-form-waypoints-to option').eq(new_index).before($(this).clone());
                    $(this).remove();
                }
            });
        }).on('click', '#bookmaps-form-dn-waypoint', function(e) {
            e.preventDefault();
            $('#bookmaps-form-waypoints-to > :selected').each(function(){
                let new_index = $('#bookmaps-form-waypoints-to option').index(this) + 1;
                if(new_index < $('#bookmaps-form-waypoints-to option').length) {
                    $('#bookmaps-form-waypoints-to option').eq(new_index).after($(this).clone());
                    $(this).remove();
                }
            });
        }).on('dblclick', '#bookmaps-form-waypoints-from option', function(e) {
            e.preventDefault();
            $(this).appendTo('#bookmaps-form-waypoints-to');
        }).on('dblclick', '#bookmaps-form-waypoints-to option', function(e) {
            e.preventDefault();
            $(this).appendTo('#bookmaps-form-waypoints-from');
        })
        $('#toolbox').append($('<a>').attr({href: '#', id:'bookmaps-btn'}).click(window.plugin.bookMaps.showDialog).text('Bookmaps'));
    }

    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();
}

var script = document.createElement('script');
script.appendChild(document.createTextNode('('+ wrapper +')({});'));
(document.body || document.head || document.documentElement).appendChild(script);