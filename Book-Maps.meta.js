// ==UserScript==
// @id             book-maps
// @name           Book-Maps
// @category       Tweaks
// @version        0.1.201806232351
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://bitbucket.org/Awleps/book-maps/raw/master/Book-Maps.meta.js
// @downloadURL    https://bitbucket.org/Awleps/book-maps/raw/master/Book-Maps.user.js
// @installURL     https://bitbucket.org/Awleps/book-maps/raw/master/Book-Maps.user.js
// @description    make G-Maps roads using Intel bookmarks
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==